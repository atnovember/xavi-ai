import * as THREE from 'three'


// Objects
const geometry = new THREE.BoxGeometry( .7, 1.2, .6, 100 );

// Materials

const material = new THREE.MeshPhongMaterial()
material.color = new THREE.Color(0x00ff00)

// Mesh
export const PlayerBox = new THREE.Mesh(geometry,material)
PlayerBox.position.set(-1.4, 0.3, 0)
PlayerBox.castShadow = true;
PlayerBox.receiveShadow = true;

export let player1BB = new THREE.Box3(new THREE.Vector3(), new THREE.Vector3())

player1BB.setFromObject(PlayerBox);
console.log('player1BB', player1BB);