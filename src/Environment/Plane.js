import * as THREE from 'three'

// add plane
const geometry1 = new THREE.PlaneBufferGeometry(10, 10);
geometry1.rotateX(4.7);
const texture1 = new THREE.TextureLoader().load('https://media.istockphoto.com/photos/green-football-stadium-field-picture-id811672528')
const material1 = new THREE.MeshStandardMaterial({ map: texture1 })
export const plane = new THREE.Mesh(geometry1, material1);
plane.position.set(0, 0, 0);
// plane.castShadow = true;
plane.receiveShadow = true;