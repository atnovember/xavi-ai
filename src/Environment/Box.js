import * as THREE from 'three'

// Objects
const geometry = new THREE.BoxGeometry( 2.2, 1.6, 3, 100 );

// Materials

const material = new THREE.MeshPhongMaterial()
material.color = new THREE.Color(0xffffff)

// Mesh
export const box = new THREE.Mesh(geometry,material)
box.position.set(5.5, 0.5, 0)
box.castShadow = true;
box.receiveShadow = true;

export let goalPost2BB = new THREE.Box3(new THREE.Vector3(), new THREE.Vector3())
goalPost2BB.setFromObject(box);
console.log('goalPost2BB', goalPost2BB);