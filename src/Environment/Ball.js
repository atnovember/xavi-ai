import * as THREE from 'three'


const geometry = new THREE.SphereGeometry( .4, 12, 8 );
const texture = new THREE.TextureLoader().load('https://static.turbosquid.com/Preview/2014/08/01__19_04_23/FootballtexturemapHighqualitythumbnail01.jpgfcd5d326-b702-4f58-91ed-49e5747e4d88DefaultHQ.jpg')
const material = new THREE.MeshPhongMaterial({ map: texture, flatShading: true })
export const ball = new THREE.Mesh( geometry, material );
ball.position.set(0, 0.3, 0);
ball.castShadow = true;
ball.receiveShadow = true;
ball.name = 'my-ball';

export let ballCollisionBox = new THREE.Sphere(ball.position, .4);
// export const ballCollisionBoxSkeleton = new THREE.SkeletonHelper( ballCollisionBox );
console.log(ballCollisionBox); 