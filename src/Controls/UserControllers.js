
import * as THREE from 'three';
import { camera } from '../Camera/MainCamera';
import { ball } from '../Environment/Ball';
import { plane } from '../Environment/Plane';

export function onClickHandler(event) {
  const x = (event.clientX / window.innerWidth) * 2 - 1;
  const y = -(event.clientY / window.innerHeight) * 2 + 1;

  const dir = new THREE.Vector3(x, y, -1)
  dir.unproject(camera)

  const ray = new THREE.Raycaster(camera.position, dir.sub(camera.position).normalize())
  const intersects = ray.intersectObject(ball);

  if ( intersects.length > 0 ) {
      const x1 = event.offsetX;
      const y1 = event.offsetY;  
      
      console.log('sphere click x, y, z', x1, y1);
    
      const center = getCenterPoint(ball);
      console.log('center ball', center, ball.position);
      ball.position.set(0.3, 0.3, 0.3);
  }
}


// TODO: почему двойной клик работает а маусклик нет?!
export function onDBClickHandler(event) {
  const x = (event.clientX / window.innerWidth) * 2 - 1;
  const y = -(event.clientY / window.innerHeight) * 2 + 1;

  var dir = new THREE.Vector3(x, y, -1)
  dir.unproject(camera)

  var ray = new THREE.Raycaster(camera.position, dir.sub(camera.position).normalize())
  var intersects = ray.intersectObject(ball);

  if ( intersects.length > 0 ) {              
      const x1 = event.offsetX;
      const y1 = event.offsetY;  
      const planeCenter = getCenterPoint(plane)

      ball.position.set(0.3, 0.4, 0.3);
  }
}


// не знаю пока что куда присунуть этот кусок...

function getCenterPoint(mesh) {
  var middle = new THREE.Vector3();
  var geometry = mesh.geometry;

  geometry.computeBoundingBox();

  middle.x = (geometry.boundingBox.max.x + geometry.boundingBox.min.x) / 2;
  middle.y = (geometry.boundingBox.max.y + geometry.boundingBox.min.y) / 2;
  middle.z = (geometry.boundingBox.max.z + geometry.boundingBox.min.z) / 2;

  mesh.localToWorld( middle );
  return middle;
}