
/**
 * 
 * @param {*} event 
 * @param {*} target 
 */
export function keyPressHandler(event, target, target2) {
  if(event.code === 'KeyE') {
    target.rotation.z += 0.1;
    target2.rotation.z += 0.1;

  }

  if(event.code === 'KeyQ') {
    target.rotation.z -= 0.1;
    target2.rotation.z -= 0.1;

  }
  
  if (event.code === 'KeyD') {
    target.position.x += 0.1
    target2.position.x += 0.1
  }

  if (event.code === 'KeyA') {
    target.position.x -= 0.1;
    target2.position.x -= 0.1
  }

  if (event.code === 'KeyW') {
    target.position.z -= 0.1;
    target2.position.z -= 0.1;
  }

  if (event.code === 'KeyS') {
    target.position.z += 0.1;
    target2.position.z += 0.1;
  }
}