import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { camera } from '../Camera/MainCamera';
import { renderer } from '../renderer';

export const controls = new OrbitControls( camera, renderer.domElement );
controls.enableDamping = true;
controls.screenSpacePanning = false;
controls.minDistance = 5;
controls.maxDistance = 10;
controls.maxPolarAngle = Math.PI / 2.25;
