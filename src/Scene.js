import * as THREE from 'three'
import { AmbientLight, PointLight } from 'three';
import { camera } from './Camera/MainCamera';
import { PlayerBox, player1BB } from './Characters/PlayerBox';
import { ball, ballCollisionBox, ballCollisionBoxSkeleton } from './Environment/Ball';
import { box, goalPost2BB } from './Environment/Box';
import { plane } from './Environment/Plane';
import { pointLight } from './Lights/PointLight';

// Scene
export const scene = new THREE.Scene()
scene.background = new THREE.Color(0x19b7f8);


/**
 * Light
 */
 scene.add(pointLight)
 /**
 * Camera
 */
// Base camera
scene.add(camera)

// Add Plane
scene.add(plane);

// Add Ball
scene.add(ball);

// Add Player
scene.add(PlayerBox);

// Add Player
scene.add(box);

// add collision boxes
// scene.add(ballCollisionBox, player1BB, goalPost2BB)
// scene.add(ballCollisionBoxSkeleton)