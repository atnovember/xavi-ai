

// // ON CLICK ANIMATION
import * as THREE from 'three'
import { camera } from "../Camera/MainCamera";
import { PlayerBox, player1BB } from "../Characters/PlayerBox";
import { ball, ballCollisionBox } from "../Environment/Ball";
import { box, goalPost2BB } from "../Environment/Box";
import { renderer } from "../renderer";
import { scene } from "../Scene";

// // linear interpolation function
// function lerp(a, b, t) {return a + (b - a) * t}

// var t = 0, dt = 0.02,                   // t (dt delta for demo)
//     a = {x: -2, y: -1, z: -1},          // start position
//     b = {x: 1.5, y: 0.5, z: 0.7};       // end position
    
// function loop(curMesh) {
//   var newX = lerp(a.x, b.x, ease(t));   // interpolate between a and b where
//   var newY = lerp(a.y, b.y, ease(t));   // t is first passed through a easing
//   var newZ = lerp(a.z, b.z, ease(t));   // function in this example.
//   sphere.position.set(newX, newY, newZ);  // set new position
//   t += dt;
//   if (t <= 0 || t >=1) dt = -dt;        // ping-pong for demo
//   renderer.render(scene, camera);
//   requestAnimationFrame(loop)
// }

// // example easing function (quadInOut, see link above)
// function ease(t) { return t<0.5 ? 2*t*t : -1+(4-2*t)*t}


function animation1() {
  PlayerBox.material.transparent = true;
  PlayerBox.material.opacity = 0.5;
  PlayerBox.material.color = new THREE.Color(Math.random() * 0xffffff);
}

function animation2() {
  ball.material.transparent = true;
  ball.material.opacity = 0.5;
  ball.material.color = new THREE.Color(Math.random() * 0xffffff);
}

export function checkCollisions() {
  console.log('checkCollisions works', ballCollisionBox.intersectsBox(player1BB))
  // console.log('checkCollisions works', ball.intersectsBox(PlayerBox))
  console.log('ballCollisionBox', ballCollisionBox.center.x)
  console.log('player1BB', player1BB.max.x)
  // Intersection, touch test
  // алгоритм для игрока и мячика
  
  if (ballCollisionBox.intersectsBox(player1BB)) {
    console.log('it mast be dribbling')
    animation1();
  } else {
    console.log('NO DRIBBLING')
    PlayerBox.material.opacity = 1.0;

    // animation2();
  }

  // if (goalPost2BB.intersectsSphere(ballCollisionBox)) {
  //   console.log('it must be goal!')
  //   animation2();
  // } else {
  //   ball.material.opacity = 1.0;
  // }

  // if(goalPost2BB.intersectsBox(goalPost2BB)) {
  //   console.log('player TOUCH the net')
  //   box.scale.y = 3;
  // } else {
  //   box.scale.y = 1;
  // }


  // // CONTAIN TEST

  // if(goalPost2BB.containsBox(player1BB)) {
  //   console.log('player inside goalpost')
  //   box.scale.y = 3;
  // } else {
  //   box.scale.y = 1;
  // }

}


export function animate() {
  player1BB.copy(PlayerBox.geometry.boundingBox).applyMatrix4(PlayerBox.matrixWorld);

  checkCollisions();

  renderer.render(scene, camera);
  requestAnimationFrame(animate);
}