import * as THREE from 'three'
import { sizes } from './constants/sizes'
import {onDBClickHandler, onClickHandler} from './Controls/UserControllers';

export const renderer = new THREE.WebGLRenderer({
   canvas: document.querySelector('canvas.webgl')
});
renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
renderer.outputEncoding = THREE.sRGBEncoding;
renderer.shadowMap.enabled = true;

renderer.domElement.addEventListener("mousedown", onClickHandler, false);
renderer.domElement.addEventListener("dblclick", onDBClickHandler, false);