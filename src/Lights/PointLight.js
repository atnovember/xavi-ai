import * as THREE from 'three'

export const pointLight = new THREE.PointLight(0xffffff, 1)
pointLight.position.x = 2
pointLight.position.y = 3
pointLight.position.z = 4

export const ambientLight = new THREE.HemisphereLight(0xffffbb, 0x080820, 1);

export const dirLight = new THREE.DirectionalLight(0xffffff, 0.3, 50);
dirLight.castShadow = true;