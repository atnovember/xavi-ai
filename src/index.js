import './style.css'
import * as THREE from 'three'
import * as dat from 'dat.gui'
import { player1BB, PlayerBox } from './Characters/PlayerBox'
import { camera } from './Camera/MainCamera'
import { ball, ballCollisionBox } from './Environment/Ball'
import { renderer } from './renderer'
import { controls } from './Controls/Orbit'
import { keyPressHandler } from './Controls/KeyPressHandler'
import { resizeHandler } from './Controls/ResizeHandlers'
import { animate, checkCollisions } from './Animation/animation'
import { scene } from './Scene'
import { goalPost2BB } from './Environment/Box'

// Debug
const gui = new dat.GUI()



window.addEventListener('keypress', (e) => keyPressHandler(e, PlayerBox, ballCollisionBox));
window.addEventListener('resize', resizeHandler)






const clock = new THREE.Clock()

const tick = () => {

    // const elapsedTime = clock.getElapsedTime()

    // Update objects
    // ball.rotation.y = .5 * elapsedTime;

    // goalPost2BB.copy(box.geometry.boundingBox).applyMatrix4(box.matrixWorld);
    // player1BB.copy(box.geometry.boundingBox).applyMatrix4(box.matrixWorld);

    // checkCollisions();

    // Update Orbital Controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()

animate(scene);